import sys

from iotAmak.amas.async_amas import AsyncAmas


class BoxAmas(AsyncAmas):

    def __init__(self, arguments: str):
        super().__init__(arguments)

    def on_initial_agents_creation(self):
        for i in range(len(self.clients)):
            self.add_agent(client_ip=self.clients[i].hostname)


if __name__ == '__main__':
    s = BoxAmas(str(sys.argv[1]))
    s.run()