import sys

from iotAmak.amas.amas import Amas


class IotAmas(Amas):

    def __init__(self, arguments: str):
        super().__init__(arguments)

    def on_initial_agents_creation(self):
        for i in range(len(self.clients)):
            self.add_agent(client_ip=self.clients[i].hostname, args=[len(self.clients)])


if __name__ == '__main__':
    s = IotAmas(str(sys.argv[1]))
    s.run()
